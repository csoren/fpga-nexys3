/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton S�rensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module five_way_buttons(
	input 			clk,
	input		[4:0] but,
	output	[4:0] down,
	output	[4:0]	down_edge);

wire[4:0] but_debounced;

debounce		#(.width(5)) Debouncer (clk, but, but_debounced);
edge_detect #(.width(5)) EdgeDetect(clk, but_debounced, down_edge);

assign down = but_debounced;

endmodule
