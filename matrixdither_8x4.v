/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton Sørensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module matrixdither_8x4(
	input[2:0] x,
	input[1:0] y,
	
	input[7:0] d,
	output[2:0] q);

function[4:0] threshold;
	input[2:0] x;
	input[1:0] y;
	
	case ({y,x})
		00: threshold = 00;
		01: threshold = 24;
		02: threshold = 04;
		03: threshold = 28;
		04: threshold = 03;
		05: threshold = 27;
		06: threshold = 07;
		07: threshold = 31;
		08: threshold = 16;
		09: threshold = 08;
		10: threshold = 20;
		11: threshold = 12;
		12: threshold = 19;
		13: threshold = 11;
		14: threshold = 23;
		15: threshold = 15;
		16: threshold = 02;
		17: threshold = 26;
		18: threshold = 06;
		19: threshold = 30;
		20: threshold = 01;
		21: threshold = 25;
		22: threshold = 05;
		23: threshold = 29;
		24: threshold = 18;
		25: threshold = 10;
		26: threshold = 22;
		27: threshold = 14;
		28: threshold = 17;
		29: threshold = 09;
		30: threshold = 21;
		31: threshold = 13;
	endcase
	
endfunction

wire[15:0] t;
assign t = 225 * d;

wire[7:0] d2;
assign d2 = t[15:8];

assign q =	
	d2[7:5] == 3'b111 ? 3'b111 :
	d2[4:0] > threshold(x,y) ? d2[7:5] + 1'b1 :
	d2[7:5];

endmodule
