/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton S�rensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module seven_segment_driver(
	input			clk1000hz,
	
	input			en3,
	input	[7:0]	d3,
	
	input			en2,
	input	[7:0]	d2,
	
	input			en1,
	input	[7:0]	d1,
	
	input			en0,
	input	[7:0]	d0,
	
	output		[7:0] seg,
	output reg	[3:0] an = 4'b1110);

wire	[7:0] current_d;
wire			current_en;

assign {current_d, current_en} =
	an[3] == 0 ? {d3, en3} :
	an[2] == 0 ? {d2, en2} :
	an[1] == 0 ? {d1, en1} :
	{d0, en0};

assign seg = current_en ? current_d : 8'hFF;

always @ (posedge clk1000hz) begin : Driver
	an <= {an[2:0], an[3]};
end

endmodule
