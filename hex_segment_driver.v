/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton S�rensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module hex_segment_driver(
	input			clk1000hz,

	input	[3:0]	d3,
	input			en3,
	input	[3:0]	d2,
	input			en2,
	input	[3:0]	d1,
	input			en1,
	input	[3:0]	d0,
	input			en0,
	
	output	[7:0]	seg,
	output	[3:0]	an);

function[7:0] segments;
	input[3:0] digit;

	case (digit)
		00: segments = ~8'b00111111;
		01: segments = ~8'b00000110;
		02: segments = ~8'b01011011;
		03: segments = ~8'b01001111;
		04: segments = ~8'b01100110;
		05: segments = ~8'b01101101;
		06: segments = ~8'b01111101;
		07: segments = ~8'b00000111;
		08: segments = ~8'b01111111;
		09: segments = ~8'b01100111;
		10: segments = ~8'b01110111;
		11: segments = ~8'b01111100;
		12: segments = ~8'b00111001;
		13: segments = ~8'b01011110;
		14: segments = ~8'b01111001;
		15: segments = ~8'b01110001;
	endcase
endfunction

wire[7:0] current_d3 = segments(d3);
wire[7:0] current_d2 = segments(d2);
wire[7:0] current_d1 = segments(d1);
wire[7:0] current_d0 = segments(d0);

seven_segment_driver SevenSegment(
	clk1000hz,
	en3, current_d3,
	en2, current_d2,
	en1, current_d1,
	en0, current_d0,
	
	seg,
	an
);

endmodule
