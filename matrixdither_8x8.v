/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton Sørensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module matrixdither_8x8(
	input[2:0] x,
	input[2:0] y,
	
	input[7:0] d,
	output[1:0] q);

function[5:0] threshold;
	input[2:0] x;
	input[2:0] y;
	
	case ({y,x})
		00: threshold = 00;
		01: threshold = 48;
		02: threshold = 13;
		03: threshold = 60;
		04: threshold = 03;
		05: threshold = 51;
		06: threshold = 15;
		07: threshold = 63;

		08: threshold = 32;
		09: threshold = 16;
		10: threshold = 44;
		11: threshold = 28;
		12: threshold = 35;
		13: threshold = 19;
		14: threshold = 47;
		15: threshold = 31;

		16: threshold = 08;
		17: threshold = 56;
		18: threshold = 04;
		19: threshold = 52;
		20: threshold = 11;
		21: threshold = 59;
		22: threshold = 07;
		23: threshold = 55;

		24: threshold = 40;
		25: threshold = 24;
		26: threshold = 36;
		27: threshold = 20;
		28: threshold = 43;
		29: threshold = 27;
		30: threshold = 39;
		31: threshold = 23;
		
		32: threshold = 02;
		33: threshold = 50;
		34: threshold = 14;
		35: threshold = 62;
		36: threshold = 01;
		37: threshold = 49;
		38: threshold = 13;
		39: threshold = 61;

		40: threshold = 34;
		41: threshold = 18;
		42: threshold = 46;
		43: threshold = 30;
		44: threshold = 33;
		45: threshold = 17;
		46: threshold = 45;
		47: threshold = 29;

		48: threshold = 10;
		49: threshold = 58;
		50: threshold = 06;
		51: threshold = 54;
		52: threshold = 09;
		53: threshold = 57;
		54: threshold = 05;
		55: threshold = 53;
		
		56: threshold = 42;
		57: threshold = 26;
		58: threshold = 38;
		59: threshold = 22;
		60: threshold = 41;
		61: threshold = 25;
		62: threshold = 37;
		63: threshold = 21;

	endcase
	
endfunction

wire[15:0] t;
assign t = 193 * d;

wire[7:0] d2;
assign d2 = t[15:8];

assign q =	
	d2[7:6] == 2'b11 ? 2'b11 :
	d2[5:0] > threshold(x,y) ? d2[7:6] + 1'b1 :
	d2[7:6];

endmodule
