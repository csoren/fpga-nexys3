/* FPGA Nexys3 library
	Copyright (C) 2013  Carsten Elton Sørensen

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module dither_888to332(
	input[7:0] dR,
	input[7:0] dG,
	input[7:0] dB,
	output[2:0] qR,
	output[2:0] qG,
	output[1:0] qB,
		
	input[2:0] x,
	input[2:0] y);
	
matrixdither_8x4 red(x, y[1:0], dR, qR);
matrixdither_8x4 green(x, y[1:0], dG, qG);
matrixdither_8x8 blue(x, y, dB, qB);

endmodule
